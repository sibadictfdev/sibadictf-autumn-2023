import random
from xml.dom import minidom

chars = {
    "0": {
        "char": 0,
        "vertexes": "111111",
        "links_ok": (
            ((1, 2), (1, 3), (2, 4), (4, 6), (3, 5), (5, 6)),
            ((2, 1), (3, 1), (4, 2), (6, 4), (5, 3), (6, 5))
        ),
        "links_error": ((2, 3), (3, 4), (4, 5), (3, 2), (4, 3), (5, 4))
    },
    "1": {
        "char": 1,
        "vertexes": "011101",
        "links_ok": (
            ((2, 3), (2, 4), (4, 6)),
            ((3, 2), (4, 2), (6, 4))
        ),
        "links_error": ((1, 2), (1, 3), (3, 4), (4, 5), (5, 6), (3, 5),
                        (2, 1), (3, 1), (4, 3), (5, 4), (6, 5), (5, 3))
    },
    "2": {
        "char": 2,
        "vertexes": "110111",
        "links_ok": (
            ((1, 2), (2, 4), (4, 5), (5, 6)),
            ((2, 1), (4, 2), (5, 4), (6, 5))
        ),
        "links_error": ((1, 3), (3, 4), (2, 3), (3, 5), (4, 6),
                        (3, 1), (4, 3), (3, 2), (5, 3), (6, 4))
    },
    "3": {
        "char": 3,
        "vertexes": "111110",
        "links_ok": (
            ((1, 2), (2, 3), (3, 4), (4, 5)),
            ((2, 1), (3, 2), (4, 3), (5, 4))
        ),
        "links_error": ((1, 3), (2, 4), (3, 5), (4, 6), (5, 6),
                        (3, 1), (4, 2), (5, 3), (6, 4), (6, 5))
    },
    "4": {
        "char": 4,
        "vertexes": "111101",
        "links_ok": (
            ((1, 3), (3, 4), (2, 4), (4, 6)),
            ((3, 1), (4, 3), (4, 2), (6, 4))
        ),
        "links_error": ((1, 2), (2, 3), (3, 5), (4, 5), (5, 6),
                        (2, 1), (3, 2), (5, 3), (5, 4), (6, 5))
    },
    "5": {
        "char": 5,
        "vertexes": "111111",
        "links_ok": (
            ((1, 2), (1, 3), (3, 4), (4, 6), (5, 6)),
            ((2, 1), (3, 1), (4, 3), (6, 4), (6, 5))
        ),
        "links_error": ((2, 3), (2, 4), (3, 5), (4, 5),
                        (3, 2), (4, 2), (5, 3), (5, 4))
    },
    "6": {
        "char": 6,
        "vertexes": "011111",
        "links_ok": (
            ((2, 3), (3, 4), (4, 6), (5, 6), (3, 5)),
            ((3, 2), (4, 3), (6, 4), (6, 5), (5, 3))
        ),
        "links_error": ((1, 2), (1, 3), (2, 4), (4, 5),
                        (2, 1), (3, 1), (4, 2), (5, 4))
    },
    "7": {
        "char": 7,
        "vertexes": "111010",
        "links_ok": (
            ((1, 2), (2, 3), (3, 5)),
            ((2, 1), (3, 2), (5, 3))
        ),
        "links_error": ((1, 3), (2, 4), (3, 4), (4, 5), (4, 6), (5, 6),
                        (3, 1), (4, 2), (4, 3), (5, 4), (6, 4), (6, 5))
    },
    "8": {
        "char": 8,
        "vertexes": "111111",
        "links_ok": (
            ((1, 2), (1, 3), (2, 4), (3, 4), (3, 5), (4, 6), (5, 6)),
            ((2, 1), (3, 1), (4, 2), (4, 3), (5, 3), (6, 4), (6, 5))
        ),
        "links_error": ((2, 3), (4, 5), (3, 2), (5, 4))
    },
    "9": {
        "char": 9,
        "vertexes": "111110",
        "links_ok": (
            ((1, 2), (1, 3), (2, 4), (3, 4), (4, 5)),
            ((2, 1), (3, 1), (4, 2), (4, 3), (5, 4))
        ),
        "links_error": ((2, 3), (3, 5), (4, 6), (5, 6),
                        (3, 2), (5, 3), (6, 4), (6, 5))
    },
}

input_str = "The history of the alternative reality of the 1950s. The gaming world is not so far from the ideals that humanity is fighting for today. A happy society, the supremacy of science, ideal cities with sun-drenched green parks and squares, automation of everyday life, and the desire to reach the stars. We can get all this in the near future. But shall we look at the heart{academy_of_consequences} underside of an ideal world? Could something like this have already happened? And what can it lead to?"
input_str_dec = [str(ord(input_str[i])) for i in range(len(input_str))]


def create_node(rt, str_numbers):
    xml_group = rt.createElement('group')
    xml.appendChild(xml_group)

    for number in str_numbers:
        xml_number = rt.createElement('number')
        xml_group.appendChild(xml_number)

        print(chars[number]["char"],end="")

        for vertex in chars[number]["vertexes"]:
            xml_vertex = rt.createElement('vertex')
            xml_number.appendChild(xml_vertex)

            xml_vertex_value = rt.createTextNode(vertex)
            xml_vertex.appendChild(xml_vertex_value)

        xml_links = rt.createElement('links')
        xml_number.appendChild(xml_links)

        random_select = random.randint(0, len(chars[number]["links_ok"]) - 1)
        for links_ref in chars[number]["links_ok"][random_select]:
            xml_links_ref = rt.createElement('vertexref')
            xml_links.appendChild(xml_links_ref)

            xml_links_ref_value = rt.createTextNode(str(links_ref))
            xml_links_ref.appendChild(xml_links_ref_value)


root = minidom.Document()

xml = root.createElement('numbers')
root.appendChild(xml)

for str_dec in input_str_dec:
    create_node(root, str_dec)

xml_str = root.toprettyxml(indent="\t")
save_path_file = "file.xml"

with open(save_path_file, "w") as f:
    f.write(xml_str)
