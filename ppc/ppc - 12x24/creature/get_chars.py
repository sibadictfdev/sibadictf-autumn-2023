from PIL import Image, ImageDraw, ImageFont
img_input = Image.open(r'../task/file.png')
img_mask = Image.open(r'../task/blank.png')
pixels = []
x, y = img_mask.size
for i in range(y):
    for j in range(x):
        print(j, i)
        pixel = img_mask.getpixel((j, i))
        if pixel == (0, 0, 0):
            pixels.append((j, i))
i = 0
for char in pixels:
    i = i + 1
    cropped_img = img_input.crop((char[0], char[1], char[0] + 12, char[1] + 24))
    cropped_img.save(f"./chars/{i}_{char[0]}_{char[1]}.png")
