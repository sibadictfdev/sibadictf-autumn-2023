from PIL import Image, ImageDraw, ImageFont
input_img = Image.open(r'../task/file.png')
output_img = Image.new('RGB', input_img.size, 'white')
symbols = [
    (483, 142),
    (365, 445),
    (335, 470),
    (359, 471),
    (149, 494),
    (269, 495),
    (566, 496),
    (192, 519),
    (369, 520),
    (170, 594),
    (463, 619),
    (700, 620),
    (243, 645),
    (599, 646),
    (170, 669),
    (441, 671),
    (156, 743),
    (487, 744),
    (123, 770),
    (321, 794)
]

i = 0
for char in symbols:
    i = i + 1
    cropped_img = input_img.crop((char[0], char[1], char[0] + 12, char[1] + 24))
    cropped_img.save(f"files/{char}_{char[0]}_{char[1]}.png")
    output_img.putpixel((char[0], char[1]), (0, 0, 0))

output_img.save(f"files/blank.png")
