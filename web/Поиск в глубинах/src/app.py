from flask import Flask, request, render_template_string
import sqlite3
import random
import string

app = Flask(__name__)

def random_string(length=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

@app.route('/', methods=['GET', 'POST'])
def home():
    user = None
    if request.method == 'POST':
        user_id = request.form.get('id')
        if user_id:
            conn = sqlite3.connect('my_database.db')
            cursor = conn.cursor()
            cursor.execute(f"SELECT * FROM users WHERE id = {user_id} AND id != 1")
            user = cursor.fetchone()
    return render_template_string("""
        <form method="POST">
            <label for="id">User ID:</label><br>
            <input type="text" id="id" name="id"><br>
            <input type="submit" value="Search">
        </form>
        {% if user %}
        <p>User: {{ user }}</p>
        {% endif %}
    """, user=user)

def init_db():
    conn = sqlite3.connect('my_database.db')
    cursor = conn.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, name TEXT)")
    cursor.execute("INSERT INTO users (name) VALUES (?)", ('heart{SQLInjectionQuest-m85b}',))
    for i in range(99):
        cursor.execute("INSERT INTO users (name) VALUES (?)", (random_string(),))
    conn.commit()

init_db()

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)