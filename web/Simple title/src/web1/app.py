from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def hello():
    return render_template('index.html', title="Welcome to OmCTF.Sib - 2023")


if __name__ == '__main__':
    app.run(debug=True, port=8801)
