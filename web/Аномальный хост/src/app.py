from flask import Flask, redirect, make_response, request

app = Flask(__name__)

@app.route('/')
def home():
    resp = make_response(redirect('/redirect1'))
    resp.set_cookie('session', '6424234-645641')
    return resp

@app.route('/redirect1')
def redirect1():
    resp = make_response(redirect('/redirect2'))
    resp.set_cookie('flag', 'https://vk.cc/cqxNyh')
    return resp

@app.route('/redirect2')
def redirect2():
    resp = make_response(redirect('/final'))
    resp.set_cookie('id', '200')
    if 'flag' in request.cookies:
        resp.delete_cookie('flag')
    return resp

@app.route('/final')
def final():
    cookie1 = request.cookies.get('session')
    cookie2 = request.cookies.get('flag')
    cookie3 = request.cookies.get('id')

    if cookie1 == '6424234-645641' and cookie2 == 'https://vk.cc/cqxNyh' and cookie3 == '200':
        return "heart{TaskForCookies-5gyh7j}"
    else:
        return "<h1>Ошибка 300: Доступ запрещен</h1>", 300

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)