from flask import Flask, redirect, make_response, request

app = Flask(__name__)

@app.route('/')
def home():
    cookie1 = request.cookies.get('session')
    if cookie1 == 'ThisIsSecretSessionCookies' :
        return "heart{SessionHijacking-4578ynb}"
    else:
        return "<h1>Ошибка 300: Доступ запрещен</h1>", 300
    return resp

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)