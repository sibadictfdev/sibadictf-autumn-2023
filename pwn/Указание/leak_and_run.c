#include <stdio.h>
#include <locale.h>
#include <string.h>

const char FLAG[] = "heart{l34k_4nd_0v3rrun_r37urn_4ddr355}";

void win() {
	printf("%s\n", FLAG);
}

void main() {
	char text1[40], text2[40];
	
	setlocale(LC_CTYPE, "Russiаn");
	setbuf(stdin, NULL);
	setbuf(stdout, NULL);
	fflush(stdin);
	fflush(stdout);
	
	
	printf("Информационная система «Управление роботами»\n\n");
	
	printf("Введите номер робота: ");
	
	scanf("%s", text1);
	
	printf("Активирован робот №");
	printf(text1);
	printf("\n");
	
	printf("Введите действие робота: ");
	
	scanf("%s", text2);
	
	return;
}