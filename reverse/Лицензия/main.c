#include <math.h>
#include <stdio.h>
#include <string.h>

#define is_digit(x) ('0' <= (x) && (x) <= '9')
#define is_upper(x) ('A' <= (x) && (x) <= 'Z')

unsigned int crc32b(unsigned char *message) {
   int i, j;
   unsigned int byte, crc, mask;

   i = 0;
   crc = 0xFFFFFFFF;
   while (message[i] != 0) {
      byte = message[i];
      crc = crc ^ byte;
      for (j = 7; j >= 0; j--) {
         mask = -(crc & 1);
         crc = (crc >> 1) ^ (0xEDB88320 & mask);
      }
      i = i + 1;
   }
   return ~crc;
}

void main() {
	int serial_number = 0, key_piece1 = 0, key_piece2;
	char serial[14];
	char true_key[14];
	char key_input[14];

	printf("Enter serial code in the following format 123456-ABCDEF\nSerial: ");
	scanf_s("%s", serial, 14);
	
	//validate
	for(int i = 0; i < 13; i++) {
		if(i < 6) {
			if(!is_digit(serial[i])) {
				printf("Serial number format error!");
				return;
			}
			serial_number += (serial[i]-'0')*pow(10,5-i);
		} else if(i > 6) {
			if(!is_upper(serial[i])) {
				printf("Serial word format error!");
				return;
			}
		}
	}
	
	key_piece1 |= (serial_number & 0xF);
	for(int i = 1; i < 6; i++ ) {
		key_piece1 = key_piece1 << 4;
		key_piece1 |= (serial_number & (0xF << 4*i)) >> 4*i;
	}
	
	sprintf(true_key, "%06X_%06X", key_piece1, key_piece1 ^ (crc32b(serial+10) & 0xFFFFFF));
	
	printf("Enter key: ");
	scanf_s("%s", key_input, 14);
	
	if(strcmp(true_key,key_input)) {
		printf("Bad key!");
	} else {
		printf("Good key!");
	}
	
	return;
}