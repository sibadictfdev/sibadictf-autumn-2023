#!/bin/bash

# Генерируем случайное количество папок и файлов
num_dirs=$((RANDOM%10+1))
num_files=$((RANDOM%10+1))

#echo "Создание $num_dirs директорий и $num_files файлов..."

for ((i=1; i<=num_dirs; i++))
do
  dir_name=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 10 | head -n 1)
  mkdir "$dir_name"
  cd "$dir_name"
  for ((j=1; j<=num_dirs; j++))
  do
    sub_dir_name=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 10 | head -n 1)
    mkdir "$sub_dir_name"
    cd "$sub_dir_name"
    # Создаем файлы в подпапках
    for ((k=1; k<=num_files; k++))
    do
      file_name=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 10 | head -n 1)
      touch "$file_name"
      # Генерируем случайное содержимое файла
      content=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $(($RANDOM % 100 + 50)) | head -n 1)
      echo $content > "$file_name"
    done
    cd ..
  done
  cd ..
done


# Создаем специальный файл с текстом 'Hello World'
special_file_name=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 10 | head -n 1)

# Выбираем случайную директорию для размещения специального файла
random_dir=$(find . -type d | shuf -n 1)
echo 'heart{FindTheFlagWithGrep-^$tbrnhi}' > "$random_dir/$special_file_name"